
/* This is an example program showing how to initialize and use the PCG64 PRNG.
 * To install do: sudo make install; Link with -lpcg_random while compiling. */ 
 
# include <stdlib.h>
# include <stdio.h>
# include <pcg64.h>

int main(void)
{
    /* Create the RNG state object */
    pcg64_rng myrng;
    
    /* Seed the RNG state with entropy bits collected by the OS */
    if(seed_rng_with_entropy(&myrng) < 0)
    {
        /* If the seeding function returns -1, then there is not enough entropy in the pool */
        fprintf(stderr, "\nSeeding from /dev/urandom failed! Exiting...\n");
        return 1; // if so, do not continue!
    }
    
    /* Right after initialization, you might want to store the RNG state */
    store_rng_state(&myrng, "seeds");
    /* The second argument is the file name where it stores the RNG state */
    /* The RNG state is a set of 8 32-bit unsigned integers */
    
    /* To read RNG state from the file "seed" on the disk use: */
    /* read_rng_state(&myrng, "seeds"); i.e., same interface as store */
    /* A negative return value from either function implies an error*/
    
    /* Let's compute the running average of a bunch of random numbers for fun */
    double sum = 0.0;
    
    for(uint32_t i0 = 0; i0 < 1048576; i0++)
    {
        /* rand_real() takes the pointer to the RNG state as its argument */
        /* and generates uniformly distributed real valued random numbers */
        /* in the range [0,1), excluding 1 */
        sum += rand_real(&myrng);
        
        /* If integers in the range [0,N) are required, use rand_bounded() */
        /* It takes two arguments: the first is the pointer to the RNG state */
        /* and the second is an unsigned integer representing the upper bound */
        /* e.g.: rand_bounded(&myrng, N) will generate random integers uniformly */
        /* distributed in the range 0 to N-1 */
        
        double avg = sum / (i0 + 1.0); // compute the running average
        
        printf("%lu\t%.12lf\n", (long unsigned) i0, avg); // print the running average
    }
    
    return 0; // successful end of program
}
