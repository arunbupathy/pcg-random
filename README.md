# pcg-random 

A minimal C implementation of Melissa O'Neill's Permuted Congruential Generator (PCG). See [her page](https://www.pcg-random.org/) for the complete C/C++ implementation. It uses a linear congruential generator (LCG) with a power-of-two modulus, whose output it *permutes* to improve the quality of randomness. It is simple in design, and yet of high quality suitable for molecular simulations. It is not the fastest generator, but is reasonably quick.

What I like about this pseudo random number generator (RNG) is that the functioning of the underlying LCG is transparent to an average user. They are extensively studied and have well understood and desirable properties when the constants are chosen properly. Further, the output function or the permutation is a simple operation that depends on the internal RNG state, but does not affect the progression of the LCG itself. This means that the desirable the state space traversal properties of the LCG are preserved, while the statistical quality of the output is improved greatly.

The 64-bit version with 128 bits of internal state is the default recommended one. The 8-bit and 16-bit variants were created for my own testing purposes only.

To install do: `sudo make install`

For usage see: `example.c`

## Other good PRNGs

The PCG webpage listed above is a good resource for other good PRNGs. A particularly good and fast one is [Bob Jenkins's small fast PRNG](http://burtleburtle.net/bob/rand/smallprng.html), which is my current favorite. The 32-bit version is great for use in GPU code. I've not included the code here as it is already simple and in C.
