/*
 * PCG Random Number Generation for C.
 *
 * Copyright 2014 Melissa O'Neill <oneill@pcg-random.org>
 * 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional information about the PCG random number generation scheme,
 * including its license and other licensing options, visit
 *
 *     http://www.pcg-random.org
 */

/* This 8 bit adaptation is by Arunkumar Bupathy <arunbupathy@gmail.com>
 * 
 * I (AB) explicitly made this 8-bit variant for testing purposes, because I
 * wanted to see to what extent the claims made by O'Neill were true.
 * In my testing, I found her claims and language to be reasonably accurate
 * and honest.
 * 
 * The critique by Sebastiano Vigna on PCG is unwarranted if you use the
 * RNG as intended. Here are some of my thoughts on his "wrap up" on PCG:
 * 
 * 1) Since I was only interested in the standard PCG variant, his comments on
 * the "ext" variants don't really bother me. Even then, proper seeding would
 * avoid this issue. Concerns of stumbling upon a bad seed are equally valid
 * for his Xo(ro)shiro family of generators.
 * 
 * 2a) His comments on streams not being "independent" is true. But in my
 * testing, generating multiple streams from the same seed does in fact
 * produce "distinct" and useful sequences of random numbers, albeit not
 * independent. O'Neill herself has addressed this issue in her writings.
 *
 * 2b) Further, I have verified O'Neill's claim that the multiple streams
 * will never overlap through the entire cycle of the generator. I have
 * tested up to 32 streams created from this 8 bit variant, using the same
 * seed, and they don't overlap ever.
 * 
 * 3) His critique on pairs of generators having the same upper state bits
 * not decorrelating till the heat death of the universe is irrelevant.
 * Again, follow correct seeding practices, and you'll be safe.
 * 
 * 4) Full disclaimer: I am more personally biased towards the underlying
 * simplistic structure of LCGs, and the clever yet simple output function
 * used by the PCG family. LFSRs do things that are not very transparent
 * for the average user to understand.
 * 
 * 5) His claims of the standard 64 bit PCG variant being "dog slow" is BS.
 * In my testing I found that Xoshiro256** was slightly faster, when you use
 * it in actual simulations. It may be 10 - 20% faster, but not worlds apart.
 */

#ifndef PCG_VARIANTS_H_INCLUDED
#define PCG_VARIANTS_H_INCLUDED 1

#define pcg8_rng pcg8_random_t

#define seed_rng pcg8_srandom_r

#define rand_int pcg8_random_r

typedef struct {
    uint16_t state;
    uint16_t inc;
} pcg8_random_t;    

#if __cplusplus
extern "C" {
#endif
    
#define PCG_MULTIPLIER_16  12829U
#define PCG_INCREMENT_16   47989U

static inline void pcg8_srandom_r(pcg8_random_t* rng,
                                    uint16_t initstate, uint16_t initseq)
{
    rng->inc = (initseq << 1u) | 1u;
    rng->state = initstate + rng->inc;
    rng->state = rng->state * PCG_MULTIPLIER_16 + rng->inc;
}

static inline uint8_t pcg8_random_r(pcg8_random_t* rng)
{
    uint16_t oldstate = rng->state;
    // Advance internal state
    rng->state = oldstate * PCG_MULTIPLIER_16 + rng->inc;
    // Calculate output function (XSH RR), uses old state for max ILP
    uint8_t value = ((oldstate >> 5u) ^ oldstate) >> 5u;
    uint32_t rot = oldstate >> 13u;
    return (value >> rot) | (value << ((- rot) & 7));
}

#endif
