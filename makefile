CC = gcc -Wall
CFLAGS = -O2 -march=native
LDFLAGS = -lm -lpcg_random
PREFIX = /usr/local

all: pcg64.o
	ar rcs libpcg_random.a pcg64.o

pcg64.o: pcg64.c pcg64.h
	$(CC) $(CFLAGS) $< -lm -c

install: all
	install libpcg_random.a $(PREFIX)/lib
	install -m 0644 pcg64.h $(PREFIX)/include

clean:
	rm -f example example.o pcg64.o libpcg_random.a

example: example.c
	$(CC) $(CFLAGS) $? $(LDFLAGS) -o $@
