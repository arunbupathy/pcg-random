/*
 * PCG Random Number Generation for C.
 *
 * Copyright 2014 Melissa O'Neill <oneill@pcg-random.org>
 * 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional information about the PCG random number generation scheme,
 * including its license and other licensing options, visit
 *
 *     http://www.pcg-random.org
 */

/* This 16 bit adaptation is by Arunkumar Bupathy <arunbupathy@gmail.com>
 * Use for testing purposes only! */

#ifndef PCG_VARIANTS_H_INCLUDED
#define PCG_VARIANTS_H_INCLUDED 1

#define pcg16_rng pcg16_random_t

#define seed_rng pcg16_srandom_r

#define rand_int pcg16_random_r

typedef struct {
    uint32_t state;
    uint32_t inc;
} pcg16_random_t;    

#if __cplusplus
extern "C" {
#endif
    
#define PCG_MULTIPLIER_32  747796405U
#define PCG_INCREMENT_32   2891336453U

static inline void pcg16_srandom_r(pcg16_random_t* rng,
                                    uint32_t initstate, uint32_t initseq)
{
    rng->inc = (initseq << 1u) | 1u;
    rng->state = initstate + rng->inc;
    rng->state = rng->state * PCG_MULTIPLIER_32 + rng->inc;
}

static inline uint16_t pcg16_random_r(pcg16_random_t* rng)
{
    uint32_t oldstate = rng->state;
    // Advance internal state
    rng->state = oldstate * PCG_MULTIPLIER_32 + rng->inc;
    // Calculate output function (XSH RR), uses old state for max ILP
    uint16_t value = ((oldstate >> 10u) ^ oldstate) >> 12u;
    uint32_t rot = oldstate >> 28u;
    return (value >> rot) | (value << ((- rot) & 15));
}

#endif
